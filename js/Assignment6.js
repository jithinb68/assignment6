var ctx = document.getElementById('myChart1');
var myChart1 = new Chart(ctx, {
    type: 'line',
    data: {
        labels: [100, 200, 300, 400, 500],
        datasets: [{
            data: [25, 70, 40, 150, 25],
            borderColor: "#ffb400",
            pointRadius: 2,
            borderWidth: 2,

        }, {
            data: [50, 150, 110, 230, 100],
            borderColor: "#14c671",
            pointRadius: 2,
            borderWidth: 2,

        }, {
            data: [100, 240, 155, 300, 350],
            borderColor: "#664eeb",
            pointRadius: 2,
            borderWidth: 2,

        }]

    },
    options: {
        elements: {
            line: {
                tension: 0
            }

        },
        scales: {
            xAxes: [{
                gridLines: {
                    display: false,
                    color: "rgba(55, 55, 55, 0.7)"
                },
                ticks: {
                    fontColor: "#d4d4d4",
                    fontSize: 9
                }
            }],
            yAxes: [{
                gridLines: {
                    drawBorder: false,
                    color: "rgba(55, 55, 55, 0.7)"
                },
                ticks: {
                    fontColor: "#d4d4d4",
                    min: 0,
                    max: 400,
                    stepSize: 100,
                    fontSize: 9
                }
            }]
        },
        legend: {
            display: false
        },
        tooltips: {
            enabled: false
        }
    }
});


var ctx = document.getElementById('myChart3');
ctx.height = 120;
var myChart3 = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: [0, 1, 2, 3, 4, 5, 6, 7, 8],
        datasets: [{
            data: [2, 5, 1, 4, 2, 2, 4, 6, 2],
            backgroundColor: 'white',

        }]
    },
    options: {
        legend: {
            display: false
        },
        scales: {
            yAxes: [{
                gridLines: {
                    drawBorder: false,
                    display: false,
                },
                ticks: {
                    display: false,
                }
            }],
            xAxes: [{
                barThickness: 6,
                gridLines: {
                    drawBorder: false,
                    display: false,
                },
                ticks: {
                    display: false,
                }

            }],

        }
    }
});

$('.searchbtn').click(function() {
    $('.searchfield').toggleClass("searchfield1");
});


$(".slider").slider({
        min: 0,
        max: 1000,
        step: 62.5,
    })
    .slider("pips", {
        rest: "label",
    })
    .slider("float", {

    });

$(".slider1").slider({
        min: 0,
        max: 120,
        step: 5
    })
    .slider("pips", {
        rest: "label",
    })
    .slider("float");